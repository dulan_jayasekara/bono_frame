package com.rezgateway.automation.input;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReaderTest {

	public InputStream getInputStream(String path) throws Exception {
		try {
			FileInputStream stream = new FileInputStream(new File(path));
			return stream;
		} catch (FileNotFoundException e) {
			throw new Exception("File not found in location :" + path);
		}
	}

	public Properties getProperties(String path) throws IOException, Exception {
		Properties prop = new Properties();
		prop.load(getInputStream(path));

		return prop;
	}

	public String getProperty(String path, String Key) throws IOException,
			Exception {
		Properties prop = new Properties();
		prop.load(getInputStream(path));

		return prop.getProperty(Key);
	}

}
